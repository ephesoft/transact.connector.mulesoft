
[![Ephesoft](docs/resources/EphesoftLogo.png "Ephesoft")](https://ephesoft.com)

## Why use Ephesoft Transact?
MuleSoft is an amazing tool for moving structured data between your critical business applications. But what do you do when your data is trapped inside of documents?

Ephesoft Transact is an Intelligent Document Processing platform that gives you the ability to automatically extract data from documents. With Transact, you can drive end-to-end automation and bring hyperautomation to new areas of your business by utilizing all of your data, even the data stored in documents.

[Ephesoft Transact - YouTube](https://www.youtube.com/watch?v=CExgdWeDbT4)

[Why Ephesoft? - YouTube](https://www.youtube.com/watch?v=_H2z3oMCyFs&list=PLlG9y2QFfsgT7tbiPaC5hDS9y51QhXdUA&index=28)

## Common Use Cases
Support for automation of front and back office processes such as:
* Accounts Payable
* Loans
* Claims
* Records digitization
* Transcripts and admissions
* Employee or customer onboarding
* Mailroom automation
* Forms intake and other processing

## About the Connector
Use the Ephesoft Transact connector to define a connection between other applications on the MuleSoft platform that have documents from which you want to extract data.

The connector can be used to configure operations to perform get and post operations for OCR/ICR conversion on image files, split and/or categorize documents by type, and extract valuable business data from scanned and digital documents.

#### Capabilities:
* Cutting-edge AI, computer vision and deep learning neural network technology power both known and unknown document type extraction
* Recognize barcodes, advanced handprint recognition technology and content-based unstructured document classification technology
* Rule-based: Key-value extraction, table extraction, cross section and pattern extraction
* REST APIs and database lookups for data validation
* Trigger and receive results from Ephesoft Transact directly from within MuleSoft Anypoint Studio
* Parse and return semi-structured or unstructured data from documents and forms using machine AI-powered capture technology
* Supports many file types including, but not limited to TIFF, PDF, JPEG, DOC, TXT, PPT, ZIP
* AI Table Rule Builder for fast table recognition setup


## About Ephesoft

Ephesoft's Intelligent Document Processing (IDP) platform and data enrichment solutions automate document-centric processes to maximize operational efficiency and human productivity for enterprises and the public sector. Using AI, patented machine learning and proprietary classification models, Ephesoft's customizable and scalable platform turns any document type into structured, actionable data to accelerate business processes and data-driven decisions. The platform's APIs and iPaaS connectors allow for fast integrations into other business systems for seamless end-to-end automation. Working with a vast partner ecosystem, Ephesoft has been deployed in the cloud and on-premises to customers around the globe saving costs, improving data accuracy and fueling their digital transformation journey towards hyperautomation. Ephesoft is headquartered in Irvine, Calif., with regional offices throughout the US, EMEA and Asia Pacific. For more information, visit ephesoft.com.

## Prerequisites
Use of the connector requires a licensed instance of Ephesoft Transact.


## Additional References
* [Connector Release Notes](docs/release-notes.md)
* [Connector Reference](docs/technical-reference.md)
* [Connector User Manual](docs/user-manual.md)
* [Intelligent Document Processing (IDP)](https://ephesoft.com/intelligent-document-processing/)
* [Ephesoft Transact](https://ephesoft.com/products/transact/)
* [Ephesoft Transact Getting Started Documentation](https://ephesoft.com/docs/products/transact/getting-started/ephesoft-transact-workflow/)
* [Ephesoft Transact WebServices Developer Documentation](https://ephesoft.com/docs/products/transact/developers/web-services-api/)
* [Contact Ephesoft Support](https://ephesoft.com/docs/products/transact/contact-support/)
* [MuleSoft's Connector Support Policy](https://www.mulesoft.com/legal/versioning-back-support-policy#anypoint-connectors)
* [MuleSoft Forum](https://help.mulesoft.com/s/forum)
