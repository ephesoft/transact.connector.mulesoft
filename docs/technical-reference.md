# Ephesoft Transact Webservice Connector Reference

The Ephesoft Webservice Connector provides connectivity to the Transact Rest API. This connector functions within a Mule application as a secure opening through which you can access.


## Configurations


| Sr.No | Name | Type | Description | Default Value | Required |
| ----------- | ----------- |----------- | ----------- |----------- | ----------- |
| 1 | Name | String | The name for this configuration. Connectors reference the configuration with this name. | | Required |
| 2 | Connection | [Basic Authentication](#connection) | The connection types to provide to this configuration. | | Required | 

### Connection 
#### Basic Authentication

|Sr.No |Name |Type |Description |Default Value |Required|
| ----------- | ----------- |----------- | ----------- |----------- | ----------- |
| 1 | Base Uri | String | Transact server instance endpoint| http://localhost:8080/dcma/ | Required |
| 2 | Username | String | Username used for authentication | ephesoft | Required |
| 3 | Password | String | Password used for authentication | 30 |
| 4 | Connection Timeout Unit | <ul><li>NANOSECONDS</li><li>MICROSECONDS</li><li>MILLISECONDS</li><li>SECONDS</li><li>MINUTES</li><li>HOURS</li><li>DAYS</li></ul>|Time unit to use in the Timeout configurations | SECONDS |  |
|5 |TLS configuration |Tls |Protocol to use for communication. Valid values are HTTP and HTTPS. Default value is HTTP. | |
|6 |Reconnection |Reconnection |When the application is deployed, a connectivity test is performed on all connectors. If set to true, deployment fails if the test doesn't pass after exhausting the associated reconnection strategy. | |


## Supported Operations


* [Upload Batch](#upload-batch)
* [Upload Batch with Priority](#upload-batch-with-priority)
* [Get Batch Instance](#get-batch-instance)
* [OCR Classify Extract](#ocr-classify-extract)
* [OCR Classify Extract Base64](#ocr-classify-extract-base64)


### Upload Batch

This web service uploads a batch for a given batch class. The uploaded file is copied to the batch class's drop folder as a single folder. The user must be authorized to execute a batch instance in that batch class otherwise an error message will be generated.<br/><br/><b>Input</b><br/>1) One file, or a zip folder containing multiple files. TIFF and PDF files are supported by default, but additional file types can be enabled by editing transact configuration files.

#### Parameters
<table>
<tr><td>Name</td><td>Type</td><td>Description</td><td>Example</td><td>Required</td></tr>
<tr>
    <td>Batch Class Identifier </td>
    <td> String</td>
    <td>Batch class to be used to process image files</td>
    <td><code>
    #[attributes.uriParams.batchClassIdentifier]
    </code>    
    </td>
    <td>Yes</td>
</tr>
<tr>
    <td>Batch Instance Name </td>
    <td> String</td>
    <td>A unique name of the batch instance</td>
    <td><code>
    #[attributes.uriParams.batchInstanceName]
    </code>    
    </td>
    <td>Yes</td>
</tr>
<tr>
    <td> Input Files </td>
    <td> Dataweave </td>
    <td> A dataweave script which outputs a json array of input files where each filename is a key and binary content is value </td>
    <td>

```xml
#[
%dw 2.0
output application/json
---
payload.parts pluck ((value, key, index) ->
{
(value.headers."Content-Disposition".filename) : value.content,
})
]
```
   </td>
    <td> Yes </td>
</tr>

</table>


#### Output
##### Status Code
``` 200 OK ```
##### Sample Response
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Web_Service_Result>
    <Batch_Instance_ID>
        <ID>BI53</ID>
    </Batch_Instance_ID>
    <Response_Code>
        <HTTP_Code>200</HTTP_Code>
        <Result>Success</Result>
    </Response_Code>
    <Result_Message>
        <Message>Batch uploaded successfully.</Message>
    </Result_Message>
</Web_Service_Result>
```


#### Throws
##### Status Code
``` 422 Unprocessable Entity```
##### Sample Response
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Web_Service_Result>
    <Error>
        <Cause>The batch name already exists. Please choose a different name and try again.</Cause>
        <Custom_Code>422014</Custom_Code>
        <More_Info>https://ephesoft.com/docs/2020-1-06/developers/web-services-api/web-services-explorer/</More_Info>
    </Error>
    <Response_Code>
        <HTTP_Code>422</HTTP_Code>
        <Result>Error occurred</Result>
    </Response_Code>
</Web_Service_Result>
```



### Upload Batch with Priority

This web service uploads a batch for a given batch class. The uploaded file is copied to the batch class's drop folder as a single folder. The user must be authorized to execute a batch instance in that batch class otherwise an error message will be generated.<br/><br/><b>Input</b><br/>1) One file, or a zip folder containing multiple files. TIFF and PDF files are supported by default, but additional file types can be enabled by editing transact configuration files.

#### Parameters
<table>
<tr><td>Name</td><td>Type</td><td>Description</td><td>Example</td><td>Required</td></tr>
<tr>
    <td>Batch Class Identifier </td>
    <td> String</td>
    <td>Batch class to be used to process image files</td>
    <td><code>
    #[attributes.uriParams.batchClassIdentifier]
    </code>    
    </td>
    <td>Yes</td>
</tr>
<tr>
    <td>Batch Instance Name </td>
    <td> String</td>
    <td>A unique name of the batch instance</td>
    <td><code>
    #[attributes.uriParams.batchInstanceName]
    </code>    
    </td>
    <td>Yes</td>
</tr>
<tr>
    <td>Batch Priority </td>
    <td> Number </td>
    <td>A manually assigned priority for batch processing </td>
    <td><code>
    #[attributes.uriParams.batchPriority]
    </code>    
    </td>
    <td>Yes</td>
</tr>
<tr>
    <td> Input Files </td>
    <td> Dataweave </td>
    <td> A dataweave script which outputs a json array of input files where each filename is a key and binary content is value </td>
    <td>

```xml
#[
%dw 2.0
output application/json
---
payload.parts pluck ((value, key, index) ->
{
(value.headers."Content-Disposition".filename) : value.content,
})
]
```
   </td>
    <td> Yes </td>
</tr>

</table>


#### Output
##### Status Code
``` 200 OK ```
##### Sample Response
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Web_Service_Result>
    <Batch_Instance_ID>
        <ID>BI53</ID>
    </Batch_Instance_ID>
    <Response_Code>
        <HTTP_Code>200</HTTP_Code>
        <Result>Success</Result>
    </Response_Code>
    <Result_Message>
        <Message>Batch uploaded successfully.</Message>
    </Result_Message>
</Web_Service_Result>
```


#### Throws
##### Status Code
``` 422 Unprocessable Entity```
##### Sample Response
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Web_Service_Result>
    <Error>
        <Cause>The batch name already exists. Please choose a different name and try again.</Cause>
        <Custom_Code>422014</Custom_Code>
        <More_Info>https://ephesoft.com/docs/2020-1-06/developers/web-services-api/web-services-explorer/</More_Info>
    </Error>
    <Response_Code>
        <HTTP_Code>422</HTTP_Code>
        <Result>Error occurred</Result>
    </Response_Code>
</Web_Service_Result>
```


### Get Batch Instance

This API returns batch instance details by passing the batch instance ID as input.

#### Parameters
<table>
<tr><td>Name</td><td>Type</td><td>Description</td><td>Example</td><td>Required</td></tr>
<tr>
    <td> Batch Instance Id </td>
    <td> String </td>
    <td> Batch instance Id of an uploaded batch </td>
    <td><code>
    #[attributes.uriParams.batchInstanceId]
    </code>    
    </td>
    <td>Yes</td>
</tr>
</table>


#### Output
##### Status Code
``` 200 OK ```
##### Sample Response
```json
{
  "message": "Batch Instance fetched successfully",
  "batchInstance": {
    "name": "bInst1",
    "identifier": "BI53",
    "description": "bInst1",
    "creationDate": "04/10/2022 11:38:35",
    "lastModified": "04/10/2022 11:40:25",
    "batchClassId": "BC6",
    "status": "FINISHED",
    "priority": 1,
    "validationOperatorName": "ephesoft",
    "executedModules": "2;1;3;5;4;8;6;7;",
    "executingServer": "localhost:8080/dcma",
    "localFolder": "C:\\Ephesoft\\SharedFolders\\ephesoft-system-folder",
    "isSuspend": false,
    "serverIP": "localhost",
    "batchRetries": 0,
    "uncSubfolder": "C:\\Ephesoft\\SharedFolders\\document-processor-import\\bInst1",
    "workFlowStartTime": "04/10/2022 11:38:55"
  }
}
```


#### Throws
##### Status Code
``` 404 Not Found```
##### Sample Response
```json
{
  "errorCode": "12.37",
  "description": "Batch Instance not found.",
  "errorMessages": [
    "Batch Instance not found."
  ],
  "moreInfo": "EC2AMAZ-M6CEBBK:8080/dcma/rest/swagger-ui.html#/v3"
}
```


### OCR Classify Extract

This web service was designed primarily to work with the Nintex workflow engine. It performs OCR, classification and extraction on the supplied input documents. Classification is based only on the first page of each file before extracting the index fields defined in the corresponding batch class. Classification and extraction are performed based on the plugins configured in the corresponding batch class, with two exceptions: 1) The PAGE_PROCESS_SCRIPTING_PLUGIN plugin will not be executed; and 2) Table extraction will be performed if the plugin is turned on, but extracted table data will not be returned in the response.<br/><br/>Only one file can be supplied as input, but multiple files can be processed at one time by combining them in a zip file and submitting the zip file to the web service.  However, note that each physical file inside the zip file will be treated as a single logical file, and will be classified based on the contents of the first page only (<u>no separation will occur within the individual files</u>).<br/><br/>This web service supports using a PDF file's EText layer for OCR and extraction, provided that the batch class has been configured accordingly to support that processing method.

#### Parameters
<table>
<tr><td>Name</td><td>Type</td><td>Description</td><td>Example</td><td>Required</td></tr>
<tr>
    <td>Batch Class Identifier </td>
    <td> String</td>
    <td>Batch class to be used to process image files</td>
    <td><code>
    #[payload.parts.batchClassIdentifier.content]
    </code>    
    </td>
    <td>Yes</td>
</tr>
<tr>
    <td> Input File </td>
    <td> Dataweave </td>
    <td> A dataweave script which outputs a json with input files where filename is a key and binary file content is the value </td>
    <td>

```xml
#[
        %dw 2.0
        output application/json
        ---
        {
        (payload.parts.inputFile.headers."Content-Disposition".filename) : payload.parts.inputFile.content
        }
]
```
   </td>
    <td> Yes </td>
</tr>
</table>


#### Output
##### Status Code
``` 200 OK ```
##### Sample Response
```json
{
  "Type": "InvoiceDoc",
  "Confidence": 90.00,
  "DocumentLevelField": [
    {
      "Name": "InvoiceNo",
      "Value": "892746352",
      "Confidence": 90.0,
      "OcrConfidenceThreshold": 10.0,
      "OcrConfidence": 90.0
    }
  ]
}
```


#### Throws
##### Status Code
``` 422 Unprocessable Entity```
##### Sample Response
```json
{
  "Cause": "No file was uploaded with request. Expecting some files as input.",
  "Custom_Code": 422730,
  "More_Info": "https://ephesoft.com/docs/2020-1-06/developers/web-services-api/web-services-explorer/"
}
```


### OCR Classify Extract Base64

This web service performs the same functionality as the ocrClassifyExtract web service, but it uses a Base64-encoded string as input. Many cloud technologies today (such as Box, Salesforce, and Microsoft Flow) stream data using Base64 encoding. By supporting a Base64-encoded input format, this web service provides greater flexibility to integrate Ephesoft Transact advanced capture capabilities into users' custom solutions.<br/><br/>The Ocr Classify Extract Base64 web service performs OCR, classification and extraction on the supplied input documents. Classification is based only on the first page of each file before extracting the index fields defined in the corresponding batch class. Classification and extraction are performed based on the plugins configured in the corresponding batch class, with two exceptions: 1) The PAGE_PROCESS_SCRIPTING_PLUGIN plugin will not be executed; and 2) Table extraction will be performed if the plugin is turned on, but extracted table data will not be returned in the response.<br/><br/>Only one file can be supplied as input, but multiple files can be processed at one time by combining them in a zip file and submitting the zip file to the web service. However, note that each physical file inside the zip file will be treated as a single logical file, and will be classified based on the contents of the first page only (<u>no separation will occur within the individual files</u>).<br/><br/>This web service supports using a PDF file's EText layer for OCR and extraction, provided that the batch class has been configured accordingly to support that processing method.

#### Parameters
<table>
<tr><td>Name</td><td>Type</td><td>Description</td><td>Example</td><td>Required</td></tr>
<tr>
    <td> Body </td>
    <td> Dataweave </td>
    <td> A dataweave script which expects the input is a json with batchClassIdentifier and fileContent attributes where fileContent is a base64 encoded content of a file</td>
    <td>

```xml
#[
        %dw 2.0
        output application/json
        ---
        {
        "batchClassIdentifier": "BC6",
        "fileContent": "<Base64 encoded file content>"
        }
]
```
   </td>
    <td> Yes </td>
</tr>
</table>


#### Output
##### Status Code
``` 200 OK ```
##### Sample Response
```json
{
  "Type": "InvoiceDoc",
  "Confidence": 90.00,
  "DocumentLevelField": [
    {
      "Name": "InvoiceNo",
      "Value": "892746352",
      "Confidence": 90.0,
      "OcrConfidenceThreshold": 10.0,
      "OcrConfidence": 90.0
    }
  ]
}
```


#### Throws
##### Status Code
``` 422 Unprocessable Entity```
##### Sample Response
```json
{
  "Cause": "No file was uploaded with request. Expecting some files as input.",
  "Custom_Code": 422730,
  "More_Info": "https://ephesoft.com/docs/2020-1-06/developers/web-services-api/web-services-explorer/"
}
```
