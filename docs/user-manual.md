# Ephesoft Transact Webservice Connector User Manual

This example project illustrates how to use Mule ESB to leverage Transact OCR APIs using Ephesoft Transact Webservice
Mulesoft Connector. After reading this document, and creating and running the example, you should be able to process
documents and retrieve extracted OCR data using various APIs provided with the connector.

## Assumptions

This document describes the details of the example within the context of Anypoint Studio, Mule graphical user
interface (GUI).

- This document assumes that you are familiar with Mule ESB and the Anypoint Studio interface.
- Before running examples it is assumed that a running Ephesoft Transact Server Instance is accessible to the server
  where these mulesoft examples are running. All examples below are using transact instance with URL http://localhost:8080/dcma 
  which can be changed to the URL for the Transact server provided to the user.
- This document assumes that User has created a BatchClass in the Transact Server instance and has an Identifier for the
  BatchClass to use in these examples.
- This document assumes that before running these tests, user has access to the username and password for Transact
  Webservices.

## Upload Batch for OCR

![Upload Batch Operation Usage](resources/BatchUploadOperation.png "Upload Batch")

### Example Use Case

In this example, a user submits one or more image (.tif, .tiff, .pdf) files or a zip file containing multiple image
files in it to the Listener using **multipart/form-data** request body. Along with the request body user sends two path
parameters **batchClassIdentifier** and **batchInstanceName**.
**Upload Batch** API operation extract all path parameters and all the files using **payload** and resubmits those files
along with the path parameters to the Transact Instance using a Web API.

### Set Up and Run Example - Upload Batch

As with other examples, you can create template applications straight out of the box in Anypoint Studio. You can tweak
the configurations of these use case-based examples to create your own customized applications in Mule.

#### Setup HTTP Listener - Upload Batch

Open the Ephesoft Mule Connector Example project in Anypoint Studio from Anypoint Exchange. In your application in
Studio, click the Global Elements tab. Double-click the HTTP Listener global element to open its Global Element
Properties panel. Change the contents of the port field to required HTTP port (e.g., 8081). Set the path element in the
Http listener to something like this, /rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}. Request must contain
Batch Class Identifier which was obtained from the Transact and a unique Batch Instance name to be passed as the path
parameters.

![Http Listener Configuration](resources/BatchUploadListener.png "Http Listener Configuration")

#### Setup Transact Upload Batch Webservice Operation

Install Ephesoft Transact Web Services Connector from Anypoint exchange. From the Mule Palette Select **Ephesoft
Transact Web Services Connector** and then select **Upload Batch** operation

- Create a new Connector Configuration and add
    - Base Uri for Transact application e.g. http://localhost:8080/dcma
    - Username and Password for transact to connect to the Transact APIs.
- Configure **Batch Class Identifier** parameter
- Configure **Batch Instance Name** parameter
- Configure **Input files** to extract all the files from payload and add those to a JSON Array.
- This operation invokes /rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName} API in Transact with
  multipart/form-data body which posts all the files defined in Input Files.
- Actual OCR operation is Asynchronous and this API returns an Identifier which can be used with other operations to get
  the status of submitted batch.

![Upload Batch Webservice Operation Configuration](resources/BatchUpload.png "Upload Batch Operation Configuration")

#### XML Flow for this example

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>
  <flow name="UploadBatch" doc:id="ba949297-38ce-4ced-a311-516639f6f139" doc:description="# This is a test">
    <http:listener doc:name="Listener" doc:id="5d1602ff-afe1-41a1-a63e-85a467ffbf5a" config-ref="HTTP_Listener_config" path="/rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}"/>
    <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name doc:name="Upload Files To Transact" doc:id="305901f6-571c-4fe9-808c-2cfd47ce2daf" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" batchClassIdentifierUriParam="#[attributes.uriParams.batchClassIdentifier]" batchInstanceNameUriParam="#[attributes.uriParams.batchInstanceName]">
      <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-input-files-bodies ><![CDATA[#[%dw 2.0
output application/json
---
payload.parts pluck ((value, key, index) -> 
{
    (value.headers."Content-Disposition".filename) : value.content,
})]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-input-files-bodies>
    </ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name>
  </flow>   
    
</mule>
```

In the Package Explorer pane in Studio, right-click the project name, then select Run As > Mule Application.

#### Testing Upload Batch

In order to test the application Postman or any REST client capable of sending multipart/form-data requests can be used,

- Add a new Request in Postman and set the request to POST.
- Set the URL to http://localhost:8081/rest/uploadBatch/BC6/bInst1.
- In this URL path value _BC6_ is the Batch Class Identifier obtained from Transact.
- Path value _bInst1_ is a unique batch instance name.
- In the request body select form-data type.
- In the body select one or more files to perform OCR.
- Click Send.
- User will get a Response that Batch has been uploaded successfully and a unique Batch Instance Identifier which can be
  used to track the status of uploaded batch using [Get Batch Instance](#get-batch-instance) operation.
- Following is the response sample,

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Web_Service_Result>
    <Batch_Instance_ID>
        <ID>BI53</ID>
    </Batch_Instance_ID>
    <Response_Code>
        <HTTP_Code>200</HTTP_Code>
        <Result>Success</Result>
    </Response_Code>
    <Result_Message>
        <Message>Batch uploaded successfully.</Message>
    </Result_Message>
</Web_Service_Result>
```

- A 200, Success response in the <Response_Code> suggests that request was submitted successfully.
- Record value returned in Batch_Instance_ID.ID parameter i.e. BI53 in this case.
- Batch Instance Identifier can be used to track the status of uploaded batch, see the next operation example.
- From here, in order to get final extraction files, i.e. PDFs and batch.xml file, Ephesoft Transact server can be setup
  to trigger a workflow in Mulesoft which is shown in following example.

## Upload Batch With Priority for OCR

![Upload Batch with Priority Operation Usage](resources/BatchUploadWithPriorityOperation.png "Upload Batch with Priority")

This operation works exactly like "Upload Batch for OCR" and there is only one minor addition in this operation. This
operation adds a batch priority along with the request which defines the priority at which submitted batch should
execute.

### Example Use Case

In this example, a user submits one or more image (.tif, .tiff, .pdf) files or a zip file containing multiple image
files in it to the Listener using **multipart/form-data** request body. Along with the request body user sends three
path parameters **batchClassIdentifier** and **batchInstanceName** and **batchPriority**.
**Upload Batch** API operation extract all path parameters and all the files using **payload** and resubmits those files
along with the path parameters to the Transact Instance using a Web API.

### Set Up and Run Example - Upload Batch with Priority

User could follow the same steps to run the examples as done
in [Set Up and Run Example - Upload Batch](#set-up-and-run-example---upload-batch). For this operation only Batch
Priority path parameter has been added.

#### Setup HTTP Listener - Upload Batch with Priority

In case Upload Batch with Priority addition path parameter is added to the HTTP listener request, rest of the
configuration is the same as [Upload Batch Http Configuration](#setup-http-listener---upload-batch) and to the operation
input as shown below,

![Http Listener with Priority Configuration](resources/BatchUploadWithPriorityListener.png "Http Listener Configuration")

#### Setup Transact Upload Batch with Priority Webservice Operation

Configuration for this operation is exactly the same
as [Upload Batch](#setup-transact-upload-batch-webservice-operation) with the addition of "Batch Priority" attribute.
![Upload Batch with Priority Webservice Operation Configuration](resources/BatchUploadWithPriority.png "Upload Batch with Priority Operation Configuration")

#### XML Flow for this example

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>  
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>  
  <flow name="UploadBatchWithPriority" doc:id="593069ad-3e09-46e8-b779-346f1319ca5e">
    <http:listener doc:name="Listener" doc:id="6de299f9-86a7-4278-b3b6-ca60c3931127" config-ref="HTTP_Listener_config" path="/rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}/{batchPriority}" />
    <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority doc:name="Upload Files To Transact with Priority" doc:id="d97ed199-4ac4-4b93-a674-0527482ed8dd" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" batchClassIdentifierUriParam="#[attributes.uriParams.batchClassIdentifier]" batchInstanceNameUriParam="#[attributes.uriParams.batchInstanceName]" batchPriorityUriParam="#[attributes.uriParams.batchPriority]">
      <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority-input-files-bodies ><![CDATA[#[%dw 2.0
output application/json
---
payload.parts pluck ((value, key, index) ->
{
    (value.headers."Content-Disposition".filename) : value.content,
})]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority-input-files-bodies>
    </ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority>
  </flow>
</mule>

```

#### Testing Upload Batch With Priority

In order to test the application Postman or any REST client capable of sending multipart/form-data requests can be used
(Also see [Testing Upload Batch](#testing-upload-batch) as same steps are followed to test this operation)

- Add a new Request in Postman and set the request to POST
- Set the URL to http://localhost:8081/rest/uploadBatch/BC6/bInst2/1
- In this URL path value _BC6_ is the Batch Class Identifier obtained from Transact.
- Path value _bInst2_ is a unique batch instance name.
- Path value _1_ is the batch priority.
- In the request body select form-data type.
- In the body select one or more files to perform OCR.
- Click Send.
- User will get a Response that Batch has been uploaded successfully and a unique Batch Instance Identifier which can be
  used to track the status of uploaded batch using [Get Batch Instance](#get-batch-instance) operation.
- Following is the response sample,

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Web_Service_Result>
  <Batch_Instance_ID>
    <ID>BI54</ID>
  </Batch_Instance_ID>
  <Response_Code>
    <HTTP_Code>200</HTTP_Code>
    <Result>Success</Result>
  </Response_Code>
  <Result_Message>
    <Message>Batch uploaded successfully.</Message>
  </Result_Message>
</Web_Service_Result>
```



## Get Batch Instance

![Get Batch Instance Operation Usage](resources/GetBatchInstanceOperation.png "Get Batch Instance")

### Example Use Case

In this example, a user retrieves the status of a Batch Instance submitted
via [Upload Batch for OCR](#upload-batch-for-ocr)
This operation uses Batch Instance Identifier returned in the response from an Upload Batch
Operation [Testing Upload Batch](#testing-upload-batch)
and this identifier used as an input parameters to the Transact Get Batch Instance Operation, which returns the last
known status of the batch. User sends **batchInstanceId** as path parameter in a http GET request to the example HTTP
listener which then gets sent to the Get Batch Instance Operation.

### Set Up and Run Example - Get Batch Instance

As with other examples, you can create template applications straight out of the box in Anypoint Studio. You can tweak
the configurations of these use case-based examples to create your own customized applications in Mule.

#### Setup HTTP Listener

Open the Ephesoft Mule Connector Example project in Anypoint Studio from Anypoint Exchange. In your application in
Studio, click the Global Elements tab. Double-click the HTTP Listener global element to open its Global Element
Properties panel. Change the contents of the port field to required HTTP port (e.g., 8081). Set the path element in the
Http listener to something like this, /rest/batchInstances/{batchInstanceId}. Request must contain Batch Instance
Identifier which was obtained from the Upload Batch request.

![Http Listener Configuration](resources/GetBatchInstanceListener.png "Http Listener Configuration")

#### Setup Transact Get Batch Instance Webservice Operation

Install Ephesoft Transact Web Services Connector from Anypoint exchange. From the Mule Palette Select **Ephesoft
Transact Web Services Connector** and then select **Get Batch Instance** operation

- Create a new (Or use existing configuration) Connector Configuration and add
    - Base Uri for Transact application e.g. http://localhost:8080/dcma
    - Username and Password for transact to connect to the Transact APIs.
- Configure **Batch Instance Id** parameter
- This operation invokes GET /rest/v3/batchInstances/{batchInstanceId} API in Transact Server with Batch Instance Id as
  a path parameter.

![Get Batch Instance Webservice Operation Configuration](resources/GetBatchInstance.png "Get Batch Instance Operation Configuration")

#### XML Flow for this example

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>
 
  <flow name="GetBatchInstance" doc:id="6e1ced75-7fbf-4c9d-8929-4c1d4788d8a0" >
    <http:listener doc:name="Listener" doc:id="17da8ed9-fc66-4113-87a8-31923a407583" config-ref="HTTP_Listener_config" path="/rest/batchInstances/{batchInstanceId}"/>
    <ephesoft-transact-web-services-connector-mule-4:get-rest-v3-batch-instances-batch-instance-id doc:name="Get Batch Instance" doc:id="d2222c57-4817-4a07-9099-00c27ae4c5d0" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" batchInstanceIdUriParam="#[attributes.uriParams.batchInstanceId]"/>
  </flow>

</mule>
```

In the Package Explorer pane in Studio, right-click the project name, then select Run As > Mule Application.

#### Testing Get Batch Instance

In order to test the application Postman or any REST client can be used,

- Add a new Request in Postman and set the request to GET
- Set the URL to http://localhost:8081/rest/batchInstances/BI53
- In this URL BI53 is the batch instance id returned from a Upload Batch or Upload Batch with Priority operation.
- Click Send.
- User will get a Response with the current status of the Instance of uploaded batch.
- Following is the response sample of a Finished batch instance which means uploaded batch has completed OCR and
  extraction and files are ready.

```json
{
  "message": "Batch Instance fetched successfully",
  "batchInstance": {
    "name": "bInst1",
    "identifier": "BI53",
    "description": "bInst1",
    "creationDate": "04/10/2022 11:38:35",
    "lastModified": "04/10/2022 11:40:25",
    "batchClassId": "BC6",
    "status": "FINISHED",
    "priority": 1,
    "validationOperatorName": "ephesoft",
    "executedModules": "2;1;3;5;4;8;6;7;",
    "executingServer": "EC2AMAZ-M6CEBBK:8080/dcma",
    "localFolder": "C:\\Ephesoft\\SharedFolders\\ephesoft-system-folder",
    "isSuspend": false,
    "serverIP": "EC2AMAZ-M6CEBBK",
    "batchRetries": 0,
    "uncSubfolder": "C:\\Ephesoft\\SharedFolders\\document-processor-import\\bInst1",
    "workFlowStartTime": "04/10/2022 11:38:55"
  }
}
```

- A 200 http response code means request was processed successfully.
- batchInstance.status shows the current status of the batch instance.
- A status of **FINISHED** means that batch is completed and files can be downloaded from Transact server.

## Transact Batch Completion Trigger

![Batch Completion Trigger Example](resources/BatchTriggerCompleteExample.png "Batch Completion Trigger")

### Example Use Case

In this example, instead of calling [Get Batch Instance](#get-batch-instance) to get the status of uploaded batch
instance [Upload Batch for OCR](#upload-batch-for-ocr), an automatic trigger can be created to start a flow in Mulesoft
on uploaded batch instance completion. In order to trigger a flow in Mulesoft a Batch Class used in the example must be
configured in Transact server with a Mulesoft endpoint. Once Batch class finishes, it invokes the configured endpoint
and contains all information about batch.xml and PDF files generated as a result of completion. Mulesoft flow in this
example is used to take all the data from Transact request and save those file on a local disk.

### Set Up and Run Example - Transact Batch Completion Trigger

As with other examples, you can create template applications straight out of the box in Anypoint Studio. You can tweak
the configurations of these use case-based examples to create your own customized applications in Mule.

#### Setup HTTP Listener

Open the Ephesoft Mule Connector Example project in Anypoint Studio from Anypoint Exchange. In your application in
Studio, click the Global Elements tab. Double-click the HTTP Listener global element to open its Global Element
Properties panel. Change the contents of the port field to required HTTP port (e.g., 8081). Set the path element in the
Http listener to /rest/v2/batchComplete. Make sure same URL is configured in Transact Server batch class which is used
to Upload Batch. Transact sends a multipart/form-data request to the configured endpoint with batchInstanceIdentifier,
batch.xml and all the PDF files.

![Batch Trigger Completion Listener Configuration](resources/BatchTriggerCompleteListener.png "Batch Trigger Completion Listener Configuration")

#### Setup Transact Batch Completion Trigger Example Flow

- From the Mule Palette > Core select Transform Message and drag it in the main designer window.
- Set the **payload** transformation (Transform Payload) of the incoming request to change the input multipart/form-data
  request to dataweave payload format.
- Add another Transform Message (Extract Files from Payload) and extract files from the payload in a json array and
  assigned it to a variable _fileArray_. Set the content of each file to base64 encoded.
- In the same Transform Message (Extract Files from Payload) set another variable _dicrectoryName_ and assign the Batch
  Instance name value to this variable.
- Now loop over all the files in the _fileArray_ variable using MuleSoft _forEach_ operation and add a transform and
  Write operation to it.
    - In the Transform Message (Set File Variables) set the _filePath_ and _fileContent_ variables extracted from _
      fileArray_. In this example local path is fixed to "C:\ephesoft-trigger-export\" which should be changed to user's
      local system path.
    - _filePath_ is build with the format, "C:\ephesoft-trigger-export\{BatchInstanceIdentifier}\{UploadedFileName}"
    - Where BatchInstanceIdentifier and UploadedFileName are sent to the flow HTTP listener in the multipart/form-data
      request by Transact.
    - Send these two variables to File Write operation (Write Files to Local Disk) so that files can be written locally.
- This operation will write all the files in a configured directory on a local disk.

#### XML Flow for this example

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  <file:config name="File_Config" doc:name="File Config" doc:id="4a4b2360-0842-4f22-9d3d-3c95e9cedeb5" />
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>
  
  <flow name="Transact-Batch-Complete-Trigger" doc:id="848634ac-a107-47ed-8c04-b2497a8e5196" >
    <http:listener doc:name="Listener" doc:id="de9bd16b-a1c4-4980-b8b3-ccd3e2f1dc46" config-ref="HTTP_Listener_config" path="/rest/v2/batchComplete"/>
    <ee:transform doc:name="Transform Payload" doc:id="34cdb834-6b87-41d5-8862-145055384f0b" >
      <ee:message >
        <ee:set-payload ><![CDATA[%dw 2.0
output application/dw
---
payload]]></ee:set-payload>
      </ee:message>
    </ee:transform>
    <ee:transform doc:name="Extract Files from Payload" doc:id="ca827fb2-60a4-4330-a5ae-02049024c887" >
      <ee:message >
      </ee:message>
      <ee:variables >
        <ee:set-variable variableName="directoryName" ><![CDATA[payload.parts.batchInstance.content as String]]></ee:set-variable>
        <ee:set-variable variableName="fileArrray" ><![CDATA[%dw 2.0
import * from dw::core::Binaries
output application/json
var uploadedFiles = payload.parts filterObject ((value, key, index) -> key as String != "batchInstance")	
---
uploadedFiles pluck ((value, key, index) -> {
	"fileName": (key),
	"content": toBase64(value.content)
})]]></ee:set-variable>
      </ee:variables>
    </ee:transform>
    <foreach doc:name="For Each" doc:id="ed24ffa5-d80f-4224-b9d4-e72c18932203" collection="#[vars.fileArrray]">
      <ee:transform doc:name="Set File Variables" doc:id="61bedc19-9cca-41b2-b59f-2690b74c1f75">
        <ee:message>
        </ee:message>
        <ee:variables >
          <ee:set-variable variableName="fileContent" ><![CDATA[%dw 2.0
import * from dw::core::Binaries
output application/octet-stream
---
fromBase64(payload.content) as Binary]]></ee:set-variable>
          <ee:set-variable variableName="filePath" ><![CDATA[%dw 2.0
output text/plain
---
('C:\\ephesoft-trigger-export\\' ++ (vars.directoryName as String) ++ '\\' ++ (payload.fileName as String))]]></ee:set-variable>
        </ee:variables>
      </ee:transform>
      <file:write doc:name="Write Files to Local Disk" doc:id="67378c6a-3d6b-4897-b496-a79a0fd23190" config-ref="File_Config" path="#[vars.filePath]">
        <file:content ><![CDATA[#[vars.fileContent]]]></file:content>
      </file:write>
    </foreach>
  </flow>
</mule>


```

In the Package Explorer pane in Studio, right-click the project name, then select Run As > Mule Application.

#### Testing Transact Batch Completion Trigger Example

- In order to test the application User must upload a batch using [Upload Batch](#upload-batch-for-ocr)
- When uploaded batch is completed then Transact Server makes a call to this workflow if URLs are configured to the
  batch classed used in the example.
- Once flow is triggered and completed, all files will be written on a local disk.

![Batch Completion Trigger Example](resources/BatchTriggerCompleteExampleFiles.png "Files saved on local disk")

## OCR Classify Extract

![OCR Classify Extract Webservice Operation](resources/OcrClassifyExtractOperation.png "OCR Classify Extract")

### Example Use Case

In this example, a user submits one image (.tif, .tiff, .pdf) files or a zip file containing multiple image files
and  **batchClassIdentifier** in it to the Listener using **multipart/form-data** request body.
**OCR Classify Extract** API operation uses the files uploaded to the listener and using **payload** and resubmits the
file along with batchClassIdentifier to the Transact Instance using a Web API. This operation can be used to extract a
single file synchronously. Transact API only returns a response when OCR is completed and extraction files are available
to download.

### Set Up and Run Example - OCR Classify Extract

As with other examples, you can create template applications straight out of the box in Anypoint Studio. You can tweak
the configurations of these use case-based examples to create your own customized applications in Mule.

#### Setup HTTP Listener

Open the Ephesoft Mule Connector Example project in Anypoint Studio from Anypoint Exchange. In your application in
Studio, click the Global Elements tab. Double-click the HTTP Listener global element to open its Global Element
Properties panel. Change the contents of the port field to required HTTP port (e.g., 8081). Set the path element in the
Http listener to something like this, /rest/ocrClassifyExtract. Request must contain Batch Class Identifier which was
obtained from the Transact and a file for OCR.

![OCR Classify Extract Listener Configuration](resources/OcrClassifyExtractListener.png "OCR Classify Extract Listener Configuration")

#### Setup OCR Classify Extract Webservice Operation

Install Ephesoft Transact Web Services Connector from Anypoint exchange. From the Mule Palette Select **Ephesoft
Transact Web Services Connector** and then select **OCR Classify Extract** operation

- Create a new Connector Configuration and add
    - Base Uri for Transact application e.g. http://localhost:8080/dcma
    - Username and Password for transact to connect to the Transact APIs.
- Configure **Batch Class Identifier** parameter
- Configure **Input file** to extract the file from payload and add those to a JSON Object.
- This operation invokes /rest/v2/ocrClassifyExtract API in Transact with multipart/form-data body which input file and
  Batch Class Identifier.
- Actual OCR operation is Synchronous and this API returns the completed OCR response instantly.

![OCR Classify Extract Operation Configuration](resources/OcrClassifyExtract.png "OCR Classify Extract Operation Configuration")

#### XML Flow for this example

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>  
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>

  <flow name="ocrClassifyExtract" doc:id="cf28287f-8bec-41b6-b967-95b16c1c9cbb" >
    <http:listener doc:name="Listener" doc:id="6ef01f4a-ad50-498f-bc75-b2558e3bc9a5" config-ref="HTTP_Listener_config" path="/rest/ocrClassifyExtract"/>
    <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract doc:name="OCR Classify Extract" doc:id="912074c9-b2ad-4437-bb7b-5bebe7b6bd53" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config">
      <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-batch-class-identifier-body ><![CDATA[#[payload.parts.batchClassIdentifier.content]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-batch-class-identifier-body>
      <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-input-file-body ><![CDATA[#[%dw 2.0
output application/json
---
{
    (payload.parts.inputFile.headers."Content-Disposition".filename) : payload.parts.inputFile.content
}]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-input-file-body>
    </ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract>
  </flow>
  
</mule>

```

In the Package Explorer pane in Studio, right-click the project name, then select Run As > Mule Application.

#### Testing OCR Classify Extract

In order to test the application Postman or any REST client capable of sending multipart/form-data requests can be used,

- Add a new Request in Postman and set the request to POST
- Set the URL to http://localhost:8081/rest/ocrClassifyExtract
- In postman add a file parameter for image file and add another parameter "batchInstanceIdentifier" for the request
  body.
- In the request body select form-data type.-
- Click Send.
- User will get a JSON response with confidence and extracted values configured for the document type.
- Following is the response sample,

```json
{
  "Type": "InvoiceDoc",
  "Confidence": 90.00,
  "DocumentLevelField": [
    {
      "Name": "InvoiceNo",
      "Value": "892746352",
      "Confidence": 90.0,
      "OcrConfidenceThreshold": 10.0,
      "OcrConfidence": 90.0
    }
  ]
}
```

## OCR Classify Extract Base64

![OCR Classify Extract Base64 Webservice Operation](resources/OcrClassifyExtractBase64Operation.png "OCR Classify Extract Base64")

### Example Use Case

In this example, a user submits one Base64 encoded image (.tif, .tiff, .pdf)  files or a zip file containing multiple
image files and  **batchClassIdentifier** to the Listener using **application/json** request body.
**OCR Classify Extract Base64** API operation uses json this json data and resubmits the Base64 encoded file along with
batchClassIdentifier to the Transact Instance using a Web API. This operation can be used to extract a single file
synchronously. Transact API only returns a response when OCR is completed and extraction files are available to
download. This operation behaves the same as [OCR Classify Extract](#ocr-classify-extract) with minor differences in
which file is submitted as Base64 and as a Json data instead of multipart/form-data.

### Set Up and Run Example - OCR Classify Extract Base64

As with other examples, you can create template applications straight out of the box in Anypoint Studio. You can tweak
the configurations of these use case-based examples to create your own customized applications in Mule.

#### Setup HTTP Listener

Open the Ephesoft Mule Connector Example project in Anypoint Studio from Anypoint Exchange. In your application in
Studio, click the Global Elements tab. Double-click the HTTP Listener global element to open its Global Element
Properties panel. Change the contents of the port field to required HTTP port (e.g., 8081). Set the path element in the
Http listener to something like this, /rest/ocrClassifyExtractBase64. Request must contain Batch Class Identifier which
was obtained from the Transact and a file for OCR.

![OCR Classify Extract Base64 Listener Configuration](resources/OcrClassifyExtractBase64Listener.png "OCR Classify Extract Base64  Listener Configuration")

#### Setup OCR Classify Extract Base64 Webservice Operation

Install Ephesoft Transact Web Services Connector from Anypoint exchange. From the Mule Palette Select **Ephesoft
Transact Web Services Connector** and then select **OCR Classify Extract Base64** operation

- Create a new Connector Configuration and add
    - Base Uri for Transact application e.g. http://localhost:8080/dcma
    - Username and Password for transact to connect to the Transact APIs.
- Configure **Batch Class Identifier** (batchClassIdentifier) parameter
- Configure **Input file** (fileContent) as a Base64 encoded value of the binary data of an image file.
- This operation invokes /rest/v2/ocrClassifyExtractBase64 API in Transact with application/Json body which input file
  and Batch Class Identifier.
- Actual OCR operation is Synchronous and this API returns the completed OCR response instantly.
- Input Request Payload to HTTP Listener may look like following,

```json
{
  "batchClassIdentifier": "BC6",
  "fileContent": "<Base64 Encoded binary Data of Image File>"
}
```

Example of a OCR Classify Extract Base64 Operation configuration shown in image below,
![OCR Classify Extract Base64 Operation Configuration](resources/OcrClassifyExtractBase64.png "OCR Classify Extract Base64 Operation Configuration")

#### XML Flow for this example

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>
  
  <flow name="ocrClassifyExtractBase64" doc:id="2e7254da-7232-4ac1-aa24-656a4e5946c5">
    <http:listener doc:name="Listener" doc:id="d676f691-f4b7-418b-bc48-cc8777d83947" config-ref="HTTP_Listener_config" path="/rest/ocrClassifyExtractBase64" />
    <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-base64 doc:name="OCR Classify Extract Base64" doc:id="a3699524-e902-4f8e-a947-a51786577f5d" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config"/>
  </flow>
</mule>
```

In the Package Explorer pane in Studio, right-click the project name, then select Run As > Mule Application.

#### Testing OCR Classify Extract Base64

In order to test the application Postman or any REST client capable of sending multipart/form-data requests can be used,

- Add a new Request in Postman and set the request to POST
- Set the URL to http://localhost:8081/rest/ocrClassifyExtractBase64
- In postman add a file parameter for image file and add another parameter "batchInstanceIdentifier" for the request
  body.
- In the request body select json type-
- Click Send.
- User will get a JSON response with confidence and extracted values configured for the document type.
- Following is the response sample,

```json
{
  "Type": "InvoiceDoc",
  "Confidence": 90.00,
  "DocumentLevelField": [
    {
      "Name": "InvoiceNo",
      "Value": "892746352",
      "Confidence": 90.0,
      "OcrConfidenceThreshold": 10.0,
      "OcrConfidence": 90.0
    }
  ]
}
```

## Complete XML Flow - Including all examples

```xml
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:ephesoft-transact-web-services-connector-mule-4="http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4" xmlns:file="http://www.mulesoft.org/schema/mule/file"
      xmlns:ee="http://www.mulesoft.org/schema/mule/ee/core" xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core" xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd
http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd
http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd
http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4 http://www.mulesoft.org/schema/mule/ephesoft-transact-web-services-connector-mule-4/current/mule-ephesoft-transact-web-services-connector-mule-4.xsd">
  <http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="5291ce1f-681b-415c-bb69-38e4ac236fb5" >
    <http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  <file:config name="File_Config" doc:name="File Config" doc:id="4a4b2360-0842-4f22-9d3d-3c95e9cedeb5" />
  <ephesoft-transact-web-services-connector-mule-4:config name="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" doc:name="Ephesoft Transact Web Services Connector Mule4Connector Config" doc:id="4399bea6-5dcd-4cd7-9d10-f20badb476ad" >
    <ephesoft-transact-web-services-connector-mule-4:basic-auth-connection username="ephesoft" password="demo" baseUri="http://localhost:8080/dcma/"/>
  </ephesoft-transact-web-services-connector-mule-4:config>
  <flow name="UploadBatch" doc:id="ba949297-38ce-4ced-a311-516639f6f139" doc:description="# This is a test">
    <http:listener doc:name="Listener" doc:id="5d1602ff-afe1-41a1-a63e-85a467ffbf5a" config-ref="HTTP_Listener_config" path="/rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}"/>
    <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name doc:name="Upload Files To Transact" doc:id="305901f6-571c-4fe9-808c-2cfd47ce2daf" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" batchClassIdentifierUriParam="#[attributes.uriParams.batchClassIdentifier]" batchInstanceNameUriParam="#[attributes.uriParams.batchInstanceName]">
      <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-input-files-bodies ><![CDATA[#[%dw 2.0
output application/json
---
payload.parts pluck ((value, key, index) -> 
{
    (value.headers."Content-Disposition".filename) : value.content,
})]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-input-files-bodies>
    </ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name>
  </flow>
  <flow name="UploadBatchWithPriority" doc:id="593069ad-3e09-46e8-b779-346f1319ca5e">
    <http:listener doc:name="Listener" doc:id="6de299f9-86a7-4278-b3b6-ca60c3931127" config-ref="HTTP_Listener_config" path="/rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}/{batchPriority}" />
    <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority doc:name="Upload Files To Transact with Priority" doc:id="d97ed199-4ac4-4b93-a674-0527482ed8dd" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" batchClassIdentifierUriParam="#[attributes.uriParams.batchClassIdentifier]" batchInstanceNameUriParam="#[attributes.uriParams.batchInstanceName]" batchPriorityUriParam="#[attributes.uriParams.batchPriority]">
      <ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority-input-files-bodies ><![CDATA[#[%dw 2.0
output application/json
---
payload.parts pluck ((value, key, index) ->
{
    (value.headers."Content-Disposition".filename) : value.content,
})]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority-input-files-bodies>
    </ephesoft-transact-web-services-connector-mule-4:post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority>
  </flow>
  <flow name="GetBatchInstance" doc:id="6e1ced75-7fbf-4c9d-8929-4c1d4788d8a0" >
    <http:listener doc:name="Listener" doc:id="17da8ed9-fc66-4113-87a8-31923a407583" config-ref="HTTP_Listener_config" path="/rest/batchInstances/{batchInstanceId}"/>
    <ephesoft-transact-web-services-connector-mule-4:get-rest-v3-batch-instances-batch-instance-id doc:name="Get Batch Instance" doc:id="d2222c57-4817-4a07-9099-00c27ae4c5d0" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config" batchInstanceIdUriParam="#[attributes.uriParams.batchInstanceId]"/>
  </flow>
  <flow name="ocrClassifyExtract" doc:id="cf28287f-8bec-41b6-b967-95b16c1c9cbb" >
    <http:listener doc:name="Listener" doc:id="6ef01f4a-ad50-498f-bc75-b2558e3bc9a5" config-ref="HTTP_Listener_config" path="/rest/ocrClassifyExtract"/>
    <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract doc:name="OCR Classify Extract" doc:id="912074c9-b2ad-4437-bb7b-5bebe7b6bd53" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config">
      <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-batch-class-identifier-body ><![CDATA[#[payload.parts.batchClassIdentifier.content]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-batch-class-identifier-body>
      <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-input-file-body ><![CDATA[#[%dw 2.0
output application/json
---
{
    (payload.parts.inputFile.headers."Content-Disposition".filename) : payload.parts.inputFile.content
}]]]></ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-input-file-body>
    </ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract>
  </flow>
  <flow name="ocrClassifyExtractBase64" doc:id="2e7254da-7232-4ac1-aa24-656a4e5946c5">
    <http:listener doc:name="Listener" doc:id="d676f691-f4b7-418b-bc48-cc8777d83947" config-ref="HTTP_Listener_config" path="/rest/ocrClassifyExtractBase64" />
    <ephesoft-transact-web-services-connector-mule-4:post-rest-v2-ocr-classify-extract-base64 doc:name="OCR Classify Extract Base64" doc:id="a3699524-e902-4f8e-a947-a51786577f5d" config-ref="Ephesoft_Transact_Web_Services_Connector_Mule4Connector_Config"/>
  </flow>
  <flow name="Transact-Batch-Complete-Trigger" doc:id="848634ac-a107-47ed-8c04-b2497a8e5196" >
    <http:listener doc:name="Listener" doc:id="de9bd16b-a1c4-4980-b8b3-ccd3e2f1dc46" config-ref="HTTP_Listener_config" path="/rest/v2/batchComplete"/>
    <ee:transform doc:name="Transform Payload" doc:id="34cdb834-6b87-41d5-8862-145055384f0b" >
      <ee:message >
        <ee:set-payload ><![CDATA[%dw 2.0
output application/dw
---
payload]]></ee:set-payload>
      </ee:message>
    </ee:transform>
    <ee:transform doc:name="Extract Files from Payload" doc:id="ca827fb2-60a4-4330-a5ae-02049024c887" >
      <ee:message >
      </ee:message>
      <ee:variables >
        <ee:set-variable variableName="directoryName" ><![CDATA[payload.parts.batchInstance.content as String]]></ee:set-variable>
        <ee:set-variable variableName="fileArrray" ><![CDATA[%dw 2.0
import * from dw::core::Binaries
output application/json
var uploadedFiles = payload.parts filterObject ((value, key, index) -> key as String != "batchInstance")	
---
uploadedFiles pluck ((value, key, index) -> {
	"fileName": (key),
	"content": toBase64(value.content)
})]]></ee:set-variable>
      </ee:variables>
    </ee:transform>
    <foreach doc:name="For Each" doc:id="ed24ffa5-d80f-4224-b9d4-e72c18932203" collection="#[vars.fileArrray]">
      <ee:transform doc:name="Set File Variables" doc:id="61bedc19-9cca-41b2-b59f-2690b74c1f75">
        <ee:message>
        </ee:message>
        <ee:variables >
          <ee:set-variable variableName="fileContent" ><![CDATA[%dw 2.0
import * from dw::core::Binaries
output application/octet-stream
---
fromBase64(payload.content) as Binary]]></ee:set-variable>
          <ee:set-variable variableName="filePath" ><![CDATA[%dw 2.0
output text/plain
---
('C:\\ephesoft-trigger-export\\' ++ (vars.directoryName as String) ++ '\\' ++ (payload.fileName as String))]]></ee:set-variable>
        </ee:variables>
      </ee:transform>
      <file:write doc:name="Write Files to Local Disk" doc:id="67378c6a-3d6b-4897-b496-a79a0fd23190" config-ref="File_Config" path="#[vars.filePath]">
        <file:content ><![CDATA[#[vars.fileContent]]]></file:content>
      </file:write>
    </foreach>
  </flow>
</mule>



```