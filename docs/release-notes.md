# Ephesoft Transact Webservices Connector Release Notes - Mule 4

----- 

Apr 15, 2022

## Version 1.0.0 - Compatibility

The Ephesoft Webservices connector is compatible with:

| Software      | Version |
| ----------- | ----------- |
| Anypoint Studio | 7.4.x |
| Mule   | 4.3.0 and later |
| JDK     | 1.8.x and later |
| Ephesoft Transact | 2020.1.02 and later |


The Ephesoft Connector provides connectivity to the Ephesoft Transact Rest APIs. This connector functions within a Mule application to consume following API from Ephesoft Transact software,

* Upload Batch - Allows uploading image files and perform OCR asynchronously.
* Upload Batch with Priority - Allows uploading image files and perform OCR asynchronously along with priority.
* Get Batch Instance - Allows to retrieve the status of a batch instance submitted asynchronously.
* OCR Classify Extract - Allows uploading image files and perform OCR synchronously.
* OCR Classify Extract Base64 - Allows uploading image files in Base64 encoded format and perform OCR synchronously.


## Version 1.0.0 - Fixed in this Release
N/A

## Support Resources

* Learn how to [Install Anypoint Connectors](https://docs.mulesoft.com/mule-runtime/3.9/installing-connectors) using Anypoint Exchange.
* Access the [MuleSoft Forum](https://help.mulesoft.com/s/forum) to pose questions and get help from Mule?s broad community of users.