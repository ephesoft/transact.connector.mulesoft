package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.operation;

import static com.mulesoft.connectivity.rest.commons.internal.RestConstants.CONNECTOR_OVERRIDES;
import static com.mulesoft.connectivity.rest.commons.internal.RestConstants.REQUEST_PARAMETERS_GROUP_NAME;

import com.mulesoft.connectivity.rest.commons.api.configuration.RestConfiguration;
import com.mulesoft.connectivity.rest.commons.api.connection.RestConnection;
import com.mulesoft.connectivity.rest.commons.api.error.RequestErrorTypeProvider;
import com.mulesoft.connectivity.rest.commons.api.operation.BaseRestOperation;
import com.mulesoft.connectivity.rest.commons.api.operation.ConfigurationOverrides;
import com.mulesoft.connectivity.rest.commons.api.operation.EntityRequestParameters;
import com.mulesoft.connectivity.rest.commons.api.operation.HttpResponseAttributes;
import com.mulesoft.connectivity.rest.commons.internal.util.RestRequestBuilder;
import com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata.PostRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityInputFilesInputMetadataResolver;
import com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata.PostRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityOutputMetadataResolver;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.OutputResolver;
import org.mule.runtime.extension.api.annotation.metadata.TypeResolver;
import org.mule.runtime.extension.api.annotation.param.Config;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.Content;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.extension.api.runtime.process.CompletionCallback;
import org.mule.runtime.extension.api.runtime.streaming.StreamingHelper;
import org.mule.runtime.http.api.HttpConstants;

public class PostRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityOperation
    extends BaseRestOperation {
  private static final String OPERATION_PATH =
      "/rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}/{batchPriority}";

  private static final RestRequestBuilder.QueryParamFormat QUERY_PARAM_FORMAT =
      RestRequestBuilder.QueryParamFormat.MULTIMAP;

  /**
   * This web service uploads a batch for a given batch class. The uploaded file is copied to the
   * batch class's drop folder as a single folder. The user must be authorized to execute a batch
   * instance in that batch class otherwise an error message will be generated.<br>
   * <br>
   * <b>Input</b><br>
   * 1) One file, or a zip folder containing multiple files. TIFF and PDF files are supported by
   * default, but additional file types can be enabled by editing the file type extension list in
   * the
   * &lt;Ephesoft_Installation&gt;\Application\WEB-INF\classes\META-INF\dcma-folder-monitor.properties
   * file.<br>
   * 2) Additional parameters described below<br>
   * <br>
   * <b>Output</b><br>
   * XML string indicating success or failure
   *
   * <p>This operation makes an HTTP POST request to the
   * /rest/uploadBatch/{batchClassIdentifier}/{batchInstanceName}/{batchPriority} endpoint
   *
   * @param config the configuration to use
   * @param connection the connection to use
   * @param batchClassIdentifierUriParam This parameter represents the identifier of the batch class
   *     to be used for processing.
   * @param batchInstanceNameUriParam This parameter represents the name of the batch instance to be
   *     created.
   * @param batchPriorityUriParam Batch Priority
   * @param postRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityInputFilesBodies The
   *     content of the 'Input Files' part.
   * @param parameters the {@link EntityRequestParameters}
   * @param overrides the {@link ConfigurationOverrides}
   * @param streamingHelper the {@link StreamingHelper}
   * @param callback the operation's {@link CompletionCallback}
   */
  @Throws(RequestErrorTypeProvider.class)
  @DisplayName("Upload Batch With Priority")
  @Summary(
      "This web service uploads a batch for a given batch class. The uploaded file is copied to the batch class's drop folder as a single folder. The user must be authorized to execute a batch instance in that batch class otherwise an error message will be generated.")
  @MediaType("application/xml")
  @OutputResolver(
      output =
          PostRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityOutputMetadataResolver
              .class)
  public void postRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriority(
      @Config RestConfiguration config,
      @Connection RestConnection connection,
      @DisplayName("Batch Class Identifier")
          @Summary(
              "This parameter represents the identifier of the batch class to be used for processing.")
          String batchClassIdentifierUriParam,
      @DisplayName("Batch Instance Name")
          @Summary("This parameter represents the name of the batch instance to be created.")
          String batchInstanceNameUriParam,
      @DisplayName("Batch Priority") String batchPriorityUriParam,
      @Content(primary = true)
          @DisplayName("Input Files")
          @Summary(
              "This parameter represents the name for the list of files. Allowed extensions are .tif, .tiff, .pdf and .zip.")
          @TypeResolver(
              PostRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityInputFilesInputMetadataResolver
                  .class)
              ParameterResolver<List<Map<String, InputStream>>>
              postRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityInputFilesBodies,
      @ParameterGroup(name = REQUEST_PARAMETERS_GROUP_NAME) EntityRequestParameters parameters,
      @ParameterGroup(name = CONNECTOR_OVERRIDES) ConfigurationOverrides overrides,
      StreamingHelper streamingHelper,
      CompletionCallback<InputStream, HttpResponseAttributes> callback) {
      // Files list is received in following json format, which is then converted into a List of Map
      // where key is file name and value is binary InputStream of the file
      // [
      //     {
      //         "filename1.tiff": <binary-content-of-file>,
      //     },
      //     {
      //         "filename2.pdf": <binary-content-of-file>,
      //     }
      //     ...
      // ]
      List<Map<String, InputStream>> valueMapList = postRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityInputFilesBodies.resolve();
      withMultipart(
              builder -> {
                  //Add each file from input as a multipart file
                  for (Map<String, InputStream> valueMap : valueMapList) {
                      for (String fileName : valueMap.keySet()) {
                          InputStream file = valueMap.get(fileName);
                          TypedValue<InputStream> typeValued = TypedValue.of(file);
                          builder.addFilePart(fileName, fileName, typeValued);
                      }
                  }
                  builder.setBoundary("__rc2_34b212");
              },
              callback,
        (body, multipartCallback) -> {
          RestRequestBuilder builder =
              new RestRequestBuilder(
                      connection.getBaseUri(),
                      OPERATION_PATH,
                      HttpConstants.Method.POST,
                      parameters)
                  .setQueryParamFormat(QUERY_PARAM_FORMAT)
                  .addHeader("content-type", "multipart/form-data; boundary=__rc2_34b212")
                  .addHeader("accept", "application/xml")
                  .addUriParam("batchClassIdentifier", batchClassIdentifierUriParam)
                  .addUriParam("batchInstanceName", batchInstanceNameUriParam)
                  .addUriParam("batchPriority", batchPriorityUriParam)
                  .setBody(body, overrides.getStreamingType());
          doRequest(
              config,
              connection,
              builder,
              overrides.getResponseTimeoutAsMillis(),
              streamingHelper,
              multipartCallback);
        });
  }
}
