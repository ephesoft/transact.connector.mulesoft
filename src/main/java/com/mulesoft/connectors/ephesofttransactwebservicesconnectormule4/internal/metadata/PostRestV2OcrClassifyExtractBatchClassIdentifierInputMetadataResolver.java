package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.input.StringInputMetadataResolver;

public class PostRestV2OcrClassifyExtractBatchClassIdentifierInputMetadataResolver
    extends StringInputMetadataResolver {
  @Override
  public String getCategoryName() {
    return "post-rest-v2-ocr-classify-extract-type-resolver";
  }
}
