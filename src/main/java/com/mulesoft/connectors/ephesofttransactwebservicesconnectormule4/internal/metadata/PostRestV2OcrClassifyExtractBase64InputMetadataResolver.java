package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.input.JsonInputMetadataResolver;

public class PostRestV2OcrClassifyExtractBase64InputMetadataResolver
    extends JsonInputMetadataResolver {
  @Override
  public String getSchemaPath() {
    return "/schemas/post-rest-v2-ocr-classify-extract-base64-input-schema.json";
  }

  @Override
  public String getCategoryName() {
    return "post-rest-v2-ocr-classify-extract-base64-type-resolver";
  }
}
