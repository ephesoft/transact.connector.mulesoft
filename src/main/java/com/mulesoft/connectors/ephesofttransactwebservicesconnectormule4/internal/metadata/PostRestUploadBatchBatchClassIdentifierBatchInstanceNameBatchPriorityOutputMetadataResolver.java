package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.output.StringOutputMetadataResolver;

public
class PostRestUploadBatchBatchClassIdentifierBatchInstanceNameBatchPriorityOutputMetadataResolver
    extends StringOutputMetadataResolver {
  @Override
  public String getCategoryName() {
    return "post-rest-upload-batch-batch-class-identifier-batch-instance-name-batch-priority-type-resolver";
  }
}
