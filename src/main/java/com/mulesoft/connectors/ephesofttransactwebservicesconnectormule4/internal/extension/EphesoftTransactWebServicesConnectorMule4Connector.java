package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.extension;

import com.mulesoft.connectivity.rest.commons.api.error.RestError;
import com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.config.EphesoftTransactWebServicesConnectorMule4Configuration;
import org.mule.runtime.api.meta.Category;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;
import org.mule.runtime.extension.api.annotation.error.ErrorTypes;
import org.mule.runtime.extension.api.annotation.license.RequiresEnterpriseLicense;

@Extension(name = "Ephesoft Transact Web Services", category = Category.CERTIFIED)
@Xml(prefix = "ephesoft-transact-web-services-connector-mule-4")
@Configurations({EphesoftTransactWebServicesConnectorMule4Configuration.class})
@ErrorTypes(RestError.class)
@RequiresEnterpriseLicense(allowEvaluationLicense = true)
public class EphesoftTransactWebServicesConnectorMule4Connector {}
