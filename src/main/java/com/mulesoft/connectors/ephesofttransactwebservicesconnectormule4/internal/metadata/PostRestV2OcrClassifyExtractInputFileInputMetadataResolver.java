package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.input.FileInputMetadataResolver;
import org.mule.metadata.api.model.MetadataFormat;

public class PostRestV2OcrClassifyExtractInputFileInputMetadataResolver
    extends FileInputMetadataResolver {
  private static final String MIME_TYPE_OCTET_STREAM = "application/octet-stream";
  @Override
  public String getCategoryName() {
    return "post-rest-v2-ocr-classify-extract-type-resolver";
  }

  @Override
  public MetadataFormat getFormat() {
    return new MetadataFormat(
            MIME_TYPE_OCTET_STREAM, MIME_TYPE_OCTET_STREAM, MIME_TYPE_OCTET_STREAM);
  }
}
