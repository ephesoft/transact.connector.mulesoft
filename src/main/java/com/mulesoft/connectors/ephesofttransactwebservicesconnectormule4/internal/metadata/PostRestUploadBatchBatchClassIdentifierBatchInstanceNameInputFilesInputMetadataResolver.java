package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.input.JsonInputMetadataResolver;

public class PostRestUploadBatchBatchClassIdentifierBatchInstanceNameInputFilesInputMetadataResolver
    extends JsonInputMetadataResolver {
  @Override
  public String getSchemaPath() {
    return "/schemas/post-rest-upload-batch-batch-class-identifier-batch-instance-name-input-files-part-input-schema.json";
  }

  @Override
  public String getCategoryName() {
    return "post-rest-upload-batch-batch-class-identifier-batch-instance-name-type-resolver";
  }
}
