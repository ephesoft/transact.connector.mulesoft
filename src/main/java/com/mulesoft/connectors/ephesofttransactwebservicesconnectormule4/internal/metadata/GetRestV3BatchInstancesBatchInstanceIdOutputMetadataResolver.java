package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.output.JsonOutputMetadataResolver;

public class GetRestV3BatchInstancesBatchInstanceIdOutputMetadataResolver
    extends JsonOutputMetadataResolver {
  @Override
  public String getSchemaPath() {
    return "/schemas/get-rest-v3-batch-instances-batch-instance-id-output-schema.json";
  }

  @Override
  public String getCategoryName() {
    return "get-rest-v3-batch-instances-batch-instance-id-type-resolver";
  }
}
