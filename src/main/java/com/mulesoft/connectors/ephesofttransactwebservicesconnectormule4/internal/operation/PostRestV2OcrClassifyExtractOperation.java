package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.operation;

import static com.mulesoft.connectivity.rest.commons.internal.RestConstants.CONNECTOR_OVERRIDES;
import static com.mulesoft.connectivity.rest.commons.internal.RestConstants.REQUEST_PARAMETERS_GROUP_NAME;

import com.mulesoft.connectivity.rest.commons.api.configuration.RestConfiguration;
import com.mulesoft.connectivity.rest.commons.api.connection.RestConnection;
import com.mulesoft.connectivity.rest.commons.api.error.RequestErrorTypeProvider;
import com.mulesoft.connectivity.rest.commons.api.operation.BaseRestOperation;
import com.mulesoft.connectivity.rest.commons.api.operation.ConfigurationOverrides;
import com.mulesoft.connectivity.rest.commons.api.operation.EntityRequestParameters;
import com.mulesoft.connectivity.rest.commons.api.operation.HttpResponseAttributes;
import com.mulesoft.connectivity.rest.commons.internal.util.RestRequestBuilder;
import com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata.PostRestV2OcrClassifyExtractBatchClassIdentifierInputMetadataResolver;
import com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata.PostRestV2OcrClassifyExtractInputFileInputMetadataResolver;
import com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata.PostRestV2OcrClassifyExtractOutputMetadataResolver;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.OutputResolver;
import org.mule.runtime.extension.api.annotation.metadata.TypeResolver;
import org.mule.runtime.extension.api.annotation.param.Config;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.Content;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.extension.api.runtime.process.CompletionCallback;
import org.mule.runtime.extension.api.runtime.streaming.StreamingHelper;
import org.mule.runtime.http.api.HttpConstants;

public class PostRestV2OcrClassifyExtractOperation extends BaseRestOperation {
  private static final String OPERATION_PATH = "/rest/v2/ocrClassifyExtract";

  private static final RestRequestBuilder.QueryParamFormat QUERY_PARAM_FORMAT =
      RestRequestBuilder.QueryParamFormat.MULTIMAP;


  /**
   * This web service was designed primarily to work with the Nintex workflow engine. It performs
   * OCR, classification and extraction on the supplied input documents. Classification is based
   * only on the first page of each file before extracting the index fields defined in the
   * corresponding batch class. Classification and extraction are performed based on the plugins
   * configured in the corresponding batch class, with two exceptions: 1) The
   * PAGE_PROCESS_SCRIPTING_PLUGIN plugin will not be executed; and 2) Table extraction will be
   * performed if the plugin is turned on, but extracted table data will not be returned in the
   * response.<br>
   * <br>
   * Only one file can be supplied as input, but multiple files can be processed at one time by
   * combining them in a zip file and submitting the zip file to the web service. However, note that
   * each physical file inside the zip file will be treated as a single logical file, and will be
   * classified based on the contents of the first page only (<u>no separation will occur within the
   * individual files</u>).<br>
   * <br>
   * This web service supports using a PDF file's EText layer for OCR and extraction, provided that
   * the batch class has been configured accordingly to support that processing method.<br>
   * <br>
   * <b>Input</b><br>
   * 1) One input file of a valid file format, or a zip file containing multiple files of valid file
   * formats<br>
   * 2) Additional parameters described below<br>
   * <br>
   * <i>Note: A list of valid file formats can be found <a
   * href="https://wiki.ephesoft.com/supported-file-types" target=_blank>here</a>. To enable support
   * for additional file formats, add the desired file extensions to the
   * <b>foldermonitor.valid_extensions</b> property in the
   * <b>Application\WEB-INF\classes\META-INF\dcma-folder-monitor\folder-monitor.properties</b>
   * file.</i><br>
   * <br>
   * <b>Output</b><br>
   * A minimized JSON string containing the classification and extraction information.
   *
   * <p>This operation makes an HTTP POST request to the /rest/v2/ocrClassifyExtract endpoint
   *
   * @param config the configuration to use
   * @param connection the connection to use
   * @param postRestV2OcrClassifyExtractBatchClassIdentifierBody The content of the 'Batch Class
   *     Identifier' part.
   * @param postRestV2OcrClassifyExtractInputFileBody The content of the 'Input File' part.
   * @param parameters the {@link EntityRequestParameters}
   * @param overrides the {@link ConfigurationOverrides}
   * @param streamingHelper the {@link StreamingHelper}
   * @param callback the operation's {@link CompletionCallback}
   */
  @Throws(RequestErrorTypeProvider.class)
  @DisplayName("OCR Classify Extract")
  @Summary(
      "This web service was designed primarily to work with the Nintex workflow engine. It performs OCR, classification and extraction on the supplied input documents. Classification is based only on the first page of each file before extracting the index fields defined in the corresponding batch class.")
  @MediaType("application/json")
  @OutputResolver(output = PostRestV2OcrClassifyExtractOutputMetadataResolver.class)
  public void postRestV2OcrClassifyExtract(
      @Config RestConfiguration config,
      @Connection RestConnection connection,
      @Content(primary = true)
          @DisplayName("Batch Class Identifier")
          @Summary(
              "This parameter represents the identifier of the batch class to be used for processing.")
          @TypeResolver(PostRestV2OcrClassifyExtractBatchClassIdentifierInputMetadataResolver.class)
          TypedValue<InputStream> postRestV2OcrClassifyExtractBatchClassIdentifierBody,
      @Content(primary = false)
          @DisplayName("Input File")
          @Summary("This parameter represents the input file to be processed.")
          @TypeResolver(PostRestV2OcrClassifyExtractInputFileInputMetadataResolver.class)
              ParameterResolver<Map<String, InputStream>> postRestV2OcrClassifyExtractInputFileBody,
      @ParameterGroup(name = REQUEST_PARAMETERS_GROUP_NAME) EntityRequestParameters parameters,
      @ParameterGroup(name = CONNECTOR_OVERRIDES) ConfigurationOverrides overrides,
      StreamingHelper streamingHelper,
      CompletionCallback<InputStream, HttpResponseAttributes> callback) {
      // Input File is received as a json value with key as filename and value as binary InputStream of the file.
      //  {
      //      "filename1.tiff": <binary-content-of-file>
      //  }
      //
      Map<String, InputStream> inputFile = (postRestV2OcrClassifyExtractInputFileBody != null)
              ? postRestV2OcrClassifyExtractInputFileBody.resolve() : null;
      withMultipart(
              builder -> {
                  builder.addPart("batchClassIdentifier", postRestV2OcrClassifyExtractBatchClassIdentifierBody);
                  //Add  file from input as a multipart file
                  if (postRestV2OcrClassifyExtractInputFileBody != null && inputFile != null) {
                      Optional<String> fileNameOp = inputFile.keySet().stream().findFirst();
                      fileNameOp.ifPresent(s -> builder.addFilePart(s, s, TypedValue.of(inputFile.get(s))));
                  }
                  builder.setBoundary("__rc2_34b212");
              },
              callback,
        (body, multipartCallback) -> {
          RestRequestBuilder builder =
              new RestRequestBuilder(
                      connection.getBaseUri(),
                      OPERATION_PATH,
                      HttpConstants.Method.POST,
                      parameters)
                  .setQueryParamFormat(QUERY_PARAM_FORMAT)
                  .addHeader("content-type", "multipart/form-data; boundary=__rc2_34b212")
                  .addHeader("accept", "application/json")
                  .setBody(body, overrides.getStreamingType());
          doRequest(
              config,
              connection,
              builder,
              overrides.getResponseTimeoutAsMillis(),
              streamingHelper,
              multipartCallback);
        });
  }
}
