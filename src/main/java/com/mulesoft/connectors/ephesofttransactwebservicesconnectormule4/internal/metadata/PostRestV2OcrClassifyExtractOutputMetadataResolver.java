package com.mulesoft.connectors.ephesofttransactwebservicesconnectormule4.internal.metadata;

import com.mulesoft.connectivity.rest.commons.api.datasense.metadata.output.JsonOutputMetadataResolver;

public class PostRestV2OcrClassifyExtractOutputMetadataResolver extends JsonOutputMetadataResolver {
  @Override
  public String getSchemaPath() {
    return "/schemas/post-rest-v2-ocr-classify-extract-output-schema.json";
  }

  @Override
  public String getCategoryName() {
    return "post-rest-v2-ocr-classify-extract-type-resolver";
  }
}
